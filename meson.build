project('libei', 'c',
  version: '0.3',
  license: 'MIT',
  default_options: [ 'c_std=gnu99', 'warning_level=2' ],
  meson_version: '>= 0.50.0')

pkgconfig = import('pkgconfig')

cc = meson.get_compiler('c')
cflags =[
    '-Wno-unused-parameter',
    '-Wmissing-prototypes',
    '-Wno-missing-field-initializers',
    '-Wstrict-prototypes',
    '-Wstrict-prototypes',
    '-Wlogical-op',
    '-Wpointer-arith',
    '-Wuninitialized',
    '-Winit-self',
    '-Wstrict-prototypes',
    '-Wimplicit-fallthrough',
    '-Wredundant-decls',
    '-Wincompatible-pointer-types',
    '-Wformat=2',
    '-Wformat-overflow=2',
    '-Wformat-signedness',
    '-Wformat-truncation=2',
    '-Wmissing-declarations',
    '-Wshift-overflow=2',
    '-Wstrict-overflow=2',
    '-Wswitch',
]

if cc.get_id() == 'clang'
    cflags += [
        # clang doesn't think just using _unref_ is a use of the variable
        #       _unref_(foo) *bar = something_that_gives_a_ref
        # but we make heavy use of that in the test suite for convenience
        # of events we know must exist but we don't care about specifically
        '-Wno-unused-variable',
    ]
endif

add_project_arguments(cc.get_supported_arguments(cflags), language: 'c')

config_h = configuration_data()
config_h.set('_GNU_SOURCE', '1')
config_h.set_quoted('EI_VERSION', meson.project_version())
config_h.set_quoted('EIS_VERSION', meson.project_version())

subdir('proto')

src_libutil = [
    'src/util-io.h',
    'src/util-io.c',
    'src/util-list.h',
    'src/util-list.c',
    'src/util-logger.h',
    'src/util-logger.c',
    'src/util-macros.h',
    'src/util-memfile.h',
    'src/util-memfile.c',
    'src/util-color.h',
    'src/util-object.h',
    'src/util-sources.h',
    'src/util-sources.c',
    'src/util-strings.c',
    'src/util-strings.h',
    'src/util-time.h',
]

lib_util = static_library('util', src_libutil)

dep_libutil = declare_dependency(link_with: lib_util)

src_libei = [
    'src/brei-shared.h',
    'src/brei-shared.c',
    'src/libei.h',
    'src/libei.c',
    'src/libei-device.c',
    'src/libei-event.c',
    'src/libei-log.c',
    'src/libei-seat.c',
    'src/libei-socket.c',
    'src/libei-fd.c',
    'src/libei-property.c',
    'src/libei-proto.h',
    'src/libei-proto.c',
    'src/libei-region.c',
    'src/libei-stubs.c',
    proto_headers,
]


if get_option('portal')
    dep_systemd = dependency('libsystemd')
    config_h.set10('ENABLE_LIBEI_PORTAL', true)
    src_libei += [
        'src/libei-portal.c',
    ]
else
    dep_systemd = dependency('', required: false)
endif

deps_libei = [
    dep_libutil,
    dep_protobuf,
    dep_systemd,
]

lib_libei = shared_library('ei',
    src_libei,
    dependencies: deps_libei,
    gnu_symbol_visibility: 'hidden',
    install: true
)
install_headers('src/libei.h')

dep_libei = declare_dependency(link_with: lib_libei,
                                include_directories: 'src')

pkgconfig.generate(lib_libei,
    filebase: 'libei',
    name: 'libEI',
    description: 'Emulated Input client library',
    version: meson.project_version(),
    libraries: lib_libei,
    variables: [ 'portal=' + get_option('portal').to_string() ],
)

src_libeis = [
    'src/brei-shared.h',
    'src/brei-shared.c',
    'src/libeis.h',
    'src/libeis.c',
    'src/libeis-client.c',
    'src/libeis-device.c',
    'src/libeis-event.c',
    'src/libeis-log.c',
    'src/libeis-property.c',
    'src/libeis-region.c',
    'src/libeis-seat.c',
    'src/libeis-socket.c',
    'src/libeis-fd.c',
    'src/libeis-proto.h',
    'src/libeis-proto.c',
    proto_headers,
]

lib_libeis = shared_library('eis',
    src_libeis,
    dependencies: [dep_libutil, dep_protobuf],
    gnu_symbol_visibility: 'hidden',
    install: true
)
install_headers('src/libeis.h')

dep_libeis = declare_dependency(link_with: lib_libeis,
                                include_directories: 'src')

pkgconfig.generate(lib_libeis,
    filebase: 'libeis',
    name: 'libEIS',
    description: 'Emulated Input server library',
    version: meson.project_version(),
    libraries: lib_libeis,
)

lib_libreis = shared_library('reis',
    'src/libreis.h',
    'src/libreis.c',
    proto_headers,
    dependencies: [dep_libutil, dep_protobuf],
    gnu_symbol_visibility: 'hidden',
    install: true,
    include_directories: 'src',
)
install_headers('src/libreis.h')

dep_libreis = declare_dependency(link_with: lib_libreis,
                                include_directories: 'src')

pkgconfig.generate(lib_libreis,
    filebase: 'libreis',
    name: 'libREIS',
    description: 'Restriction handler for Emulated Input servers',
    version: meson.project_version(),
    libraries: lib_libreis,
)

dep_libxkbcommon = dependency('xkbcommon', required: false)
config_h.set10('HAVE_LIBXKBCOMMON', dep_libxkbcommon.found())
dep_libevdev = dependency('libevdev', required: false)
config_h.set10('HAVE_LIBEVDEV', dep_libevdev.found())

src_eis_demo_server = [
    'tools/eis-demo-server.h',
    'tools/eis-demo-server.c',
]
if dep_libevdev.found()
    src_eis_demo_server += [
        'tools/eis-demo-server-uinput.c',
    ]
endif


executable('eis-demo-server',
           src_eis_demo_server,
           dependencies: [
               dep_libutil,
               dep_libeis,
               dep_libxkbcommon,
               dep_libevdev
           ])

executable('ei-demo-client',
           'tools/ei-demo-client.c',
           dependencies: [dep_libutil, dep_libei, dep_libxkbcommon])

executable('ei-debug-events',
           'tools/ei-debug-events.c',
           dependencies: [dep_libutil, dep_libei, dep_libevdev],
           install: true)

if get_option('portal')
    executable('eis-fake-portal',
               'tools/eis-fake-portal.c',
               include_directories: 'src',
               dependencies: [dep_libutil, dep_systemd, dep_libreis])
    executable('eis-fake-impl-portal',
               'tools/eis-fake-impl-portal.c',
               include_directories: 'src',
               dependencies: [dep_libutil, dep_systemd, dep_libreis])
endif

# tests
if get_option('tests')
    subproject('munit', default_options: 'werror=false')

    munit = dependency('munit', fallback: ['munit', 'munit_dep'])

    lib_unittest = static_library('unittest',
        'src/util-munit.h',
        'src/util-munit.c',
        dependencies: munit,
    )

    dep_unittest = declare_dependency(
        link_with: lib_unittest,
        dependencies: munit
    )

    test('unit-tests-utils',
        executable('unit-tests-utils',
                    'test/unit-tests.c',
                    src_libutil,
                    include_directories: 'src',
                    c_args: ['-D_enable_tests_'],
                    dependencies: [dep_unittest]))

    test('unit-tests-ei',
        executable('unit-tests-ei',
                    'test/unit-tests.c',
                    src_libei,
                    include_directories: 'src',
                    c_args: ['-D_enable_tests_'],
                    dependencies: deps_libei + [dep_unittest]))

    test('unit-tests-eis',
        executable('unit-tests-eis',
                    'test/unit-tests.c',
                    src_libeis,
                    include_directories: 'src',
                    c_args: ['-D_enable_tests_'],
                    dependencies: [dep_unittest, dep_libutil, dep_protobuf]))

    lib_eierpecken = static_library('eierpecken',
                                    'test/eierpecken.h',
                                    'test/eierpecken.c',
                                    include_directories: 'src',
                                    dependencies: [munit, dep_libutil, dep_libei, dep_libeis],
    )

    test('eierpecken',
        executable('eierpecken',
                    'test/test-main.c',
                    'test/test-ei.c',
                    'test/test-ei-device.c',
                    'test/test-ei-seat.c',
                    'test/test-eis.c',
                    link_with: lib_eierpecken,
                    dependencies: [dep_unittest, dep_libei, dep_libeis]))

    valgrind = find_program('valgrind', required : false)
    if valgrind.found()
        add_test_setup('valgrind',
                exe_wrapper : [ valgrind,
                        '--leak-check=full',
                        '--gen-suppressions=all',
                        '--error-exitcode=3' ],
                timeout_multiplier : 100)
    else
        message('valgrind not found, disabling valgrind test suite')
    endif


# build-test only
    executable('test-build-libei',
        'test/buildtest.c',
        dependencies : [dep_libei],
        include_directories : 'src',
        c_args : ['-Werror', '-DINCLUDE_LIBEI=1'],
        install : false)

    executable('test-build-libeis',
        'test/buildtest.c',
        dependencies : [dep_libeis],
        include_directories : 'src',
        c_args : ['-Werror', '-DINCLUDE_LIBEIS=1'],
        install : false)

    executable('test-build-libreis',
        'test/buildtest.c',
        dependencies : [dep_libreis],
        include_directories : 'src',
        c_args : ['-Werror', '-DINCLUDE_LIBREIS=1'],
        install : false)

    if add_languages('cpp', required: false)
        executable('test-build-cxx',
                'test/buildtest.cc',
                dependencies: [dep_libei, dep_libeis],
                include_directories: 'src',
                install: false)
    endif
endif # tests

configure_file(output: 'config.h', configuration: config_h)

if get_option('documentation')
    subdir('doc')
endif
