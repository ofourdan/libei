# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:

{# You're looking at the template here, so you can ignore the below
   warning. This is the right file to edit #}
########################################
#                                      #
# THIS FILE IS GENERATED, DO NOT EDIT  #
#                                      #
########################################

.templates_sha: &template_sha 0c312d9c7255f46e741d43bcd1930f09cd12efe7 # see https://docs.gitlab.com/ee/ci/yaml/#includefile

include:
  {% for distro in distributions|sort(attribute="name") %}
  {% if not distro.does_not_have_ci_templates %}
  # {{ distro.name.capitalize() }} container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/{{distro.name}}.yml'
  {% endif %}
  {% endfor %}

stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - build            # for actually building and testing things in a container
  - distro           # distribs test
  - container_clean  # clean up unused container images
  - deploy

variables:
  ###############################################################################
  # This is the list of packages required to build libei with the default       #
  # configuration.                                                              #
  #                                                                             #
  # Run dnf install/apt-get install/.. with the list of packages for your       #
  # distribution                                                                #
  #                                                                             #
  # See the documentation here:                                                 #
  # https://wayland.freedesktop.org/libinput/doc/latest/building_libinput.html  #
  ###############################################################################
  FEDORA_PACKAGES:    'git diffutils gcc gcc-c++ pkgconf-pkg-config meson systemd-devel protobuf-c-devel libxkbcommon-devel doxygen'
  ############################ end of package lists #############################

  # these tags should be updated each time the list of packages is updated
  # changing these will force rebuilding the associated image
  # Note: these tags have no meaning and are not tied to a particular
  # libinput version
{% for distro in distributions %}
  {{"%-13s"| format(distro.name.upper() + '_TAG:')}}'{{distro.tag}}'
{% endfor %}

  FDO_UPSTREAM_REPO: libinput/libei

  MESON_BUILDDIR: "build dir"
  NINJA_ARGS: ''
  MESON_ARGS: ''
  MESON_TEST_ARGS: ''
  GIT_DEPTH: 1

.policy:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  # cancel run when a newer version is pushed to the branch
  interruptible: true

.default_artifacts:
  artifacts:
    name: "meson-logs-$CI_JOB_NAME"
    when: always
    expire_in: 1 week
    paths:
      - $MESON_BUILDDIR/meson-logs
    reports:
      junit: $MESON_BUILDDIR/meson-logs/.junit.xml

#################################################################
#                                                               #
#                          prep stage                           #
#                                                               #
#################################################################

fail-if-fork-is-not-public:
  stage: prep
  script:
    - |
      if [ $CI_PROJECT_VISIBILITY != "public" ]; then
           echo "*************************************************************************************"
           echo "Project visibility must be set to 'public'"
           echo "Change this in $CI_PROJECT_URL/edit under 'Visibility, project features, permissions'"
           echo "*************************************************************************************"
           exit 1
      fi
  except:
    - master@libinput/libei

# Re-generate the CI script and make sure it's the one currently checked in
# If this job fails, re-generate the gitlab-ci.yml script, see
# $SRCDIR/.gitlab-ci/generate-gitlab-ci.py
#
check-ci-script:
  image: golang:alpine
  stage: prep
  before_script:
    - apk add python3 py-pip git
    - pip3 install git+http://gitlab.freedesktop.org/freedesktop/ci-templates
  script:
    - ci-fairy generate-template
    - git diff --exit-code && exit 0 || true
    - echo "Committed gitlab-ci.yml differs from generated gitlab-ci.yml. Please verify"
    - exit 1

#
# Verify that commit messages are as expected, signed-off, etc.
#

check-commit:
  image: golang:alpine
  stage: prep
  before_script:
    - apk add python3 py-pip git
    - pip3 install git+http://gitlab.freedesktop.org/freedesktop/ci-templates
  script:
    - ci-fairy check-commits --junit-xml=results.xml
  except:
    - master@libinput/libei
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#

check-merge-request:
  image: golang:alpine
  stage: deploy
  before_script:
    - apk add python3 py-pip git
    - pip3 install git+http://gitlab.freedesktop.org/freedesktop/ci-templates
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true

#
# Note: images are rebuilt weekly with a scheduled pipeline with FDO_FORCE_REBUILD set
#
#
{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@container-prep:
  extends:
    - .fdo.container-build@{{distro.name}}
    - .policy
  stage: prep
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_PACKAGES: ${{distro.name.upper()}}_PACKAGES
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
 {% if version == 'ubuntu'%}
    FDO_DISTRIBUTION_EXEC: $UBUNTU_EXEC
 {% endif %}

{% endfor %}
{% endfor %}

#################################################################
#                                                               #
#                   container clean stage                       #
#                 run during the clean stage                    #
#                                                               #
#################################################################

#
# This stage will look for the container images we currently have in
# the registry and will remove any that are not tagged with the provided
# $container_image:$tag
#
.container-clean:
  stage: container_clean
  extends:
    - .policy
  image: golang:alpine
  before_script:
    - apk add python3 py-pip git
    - pip3 install git+http://gitlab.freedesktop.org/freedesktop/ci-templates
  script:
    # Go to your Profile, Settings, Access Tokens
    # Create a personal token with 'api' scope, copy the value.
    # Go to CI/CD, Schedules, schedule a new monthly job (or edit the existing one)
    # Define a variable of type File named AUTHFILE. Content is that token
    # value.
    - ci-fairy -v --authfile $AUTHFILE delete-image
            --repository $FDO_DISTRIBUTION_NAME/$FDO_DISTRIBUTION_VERSION
            --exclude-tag $FDO_DISTRIBUTION_TAG
  dependencies: []
  allow_failure: true
  only:
    - schedules

{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@container-clean:
  extends:
    - .container-clean
  variables:
    GIT_STRATEGY: none
    {{distro.name.upper()}}_VERSION: '{{version}}'
    CURRENT_CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/{{distro.name}}/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG

{% endfor %}
{% endfor %}

#################################################################
#                                                               #
#                       build stage                             #
#                                                               #
#################################################################

.build@template:
  extends:
    - .policy
    - .default_artifacts
  stage: build
  script:
    - .gitlab-ci/meson-build.sh
  dependencies: []

#
# Fedora
#

{% for distro in distributions if distro.use_for_custom_build_tests %}
{% set version = "{}".format(distro.versions|last()) %}
.{{distro.name}}-build@template:
  extends:
    - .fdo.distribution-image@fedora
    - .build@template
  variables:
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
  needs:
    - "{{distro.name}}:{{version}}@container-prep"

default-build-release@{{distro.name}}:{{version}}:
  stage: distro
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: "-Dbuildtype=release"
    CFLAGS: "-Werror -Wno-error=vla-parameter"  # munit triggers -Wvla-parameter

build-no-libxkcommon-nodeps@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  before_script:
    - dnf remove -y libxkcommon-devel

build-no-doxygen@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: "-Ddocumentation=false"
  before_script:
    - dnf remove -y doxygen

build-no-portal@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  before_script:
    - dnf remove -y libsystemd-devel
  variables:
    MESON_ARGS: "-Dportal=false"

valgrind@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_TEST_ARGS: '--setup=valgrind'
  before_script:
    - dnf install -y valgrind

werror@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: '-Dwerror=true'
  allow_failure: true

{% endfor %}

#################################################################
#                                                               #
#                        distro stage                           #
#                                                               #
#################################################################

{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@default-build:
  stage: distro
  extends:
    - .build@template
    - .fdo.distribution-image@{{distro.name}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
    {# Where we have extra_variables defined, add them to the list #}
    {% if distro.build is defined and distro.build.extra_variables is defined %}
    {% for var in distro.build.extra_variables %}
    {{var}}
    {% endfor %}
    {% endif %}
  needs:
    - "{{distro.name}}:{{version}}@container-prep"

{% endfor %}
{% endfor %}

pages:
  stage: deploy
  extends:
    - .build@template
    - .fdo.distribution-image@{{pages.distro}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{pages.version}}'
    FDO_DISTRIBUTION_TAG: ${{pages.distro.upper()}}_TAG
    MESON_ARGS: "-Ddocumentation=true"
  after_script:
    - mv "$MESON_BUILDDIR"/doc/html/ public
  only:
    refs:
      - master
  artifacts:
    paths:
      - public
