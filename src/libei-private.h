/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <stdarg.h>

#include "util-macros.h"
#include "util-object.h"

#include "libei.h"
#include "util-list.h"
#include "util-sources.h"
#include "util-structs.h"

struct ei_backend_interface {
	void (*destroy)(struct ei *ei, void *backend);
};

enum ei_state {
	EI_STATE_NEW,		/* No backend yet */
	EI_STATE_BACKEND,	/* We have a backend */
	EI_STATE_CONNECTING,	/* client requested connect */
	EI_STATE_CONNECTED,	/* server has sent connect */
	EI_STATE_DISCONNECTING, /* in the process of cleaning up */
	EI_STATE_DISCONNECTED,
};

struct ei {
	struct object object;
	void *user_data;
	struct sink *sink;
	struct source *source;
	struct ei_backend_interface backend_interface;
	void *backend;
	enum ei_state state;
	struct list event_queue;
	struct list seats;
	char *name;

	struct list properties;

	struct {
		ei_log_handler handler;
		enum ei_log_priority priority;
	} log;

	const struct ei_proto_requests *requests;

	bool is_sender;
};

enum ei_seat_state {
	EI_SEAT_STATE_NEW,
	EI_SEAT_STATE_REMOVED,
};

struct ei_seat {
	struct object object;
	void *user_data;
	struct list link;
	enum ei_seat_state state;
	struct list devices;
	struct list devices_removed; /* removed from seat but client still has a ref */
	uint32_t id;
	uint32_t capabilities;
	uint32_t capabilities_bound;
	char *name;
};

enum ei_device_state {
	/* Before the DeviceAddedDone was received */
	EI_DEVICE_STATE_NEW,
	EI_DEVICE_STATE_PAUSED,
	EI_DEVICE_STATE_RESUMED,
	EI_DEVICE_STATE_EMULATING,
	/**
	 * Client removed the device, we no longer accept events from the
	 * client
	 */
	EI_DEVICE_STATE_REMOVED_FROM_CLIENT,
	/**
	 * Server removed the device, we need to remove it ourselves now.
	 */
	EI_DEVICE_STATE_REMOVED_FROM_SERVER,
	/**
	 * Device has been removed by both sides.
	 */
	EI_DEVICE_STATE_DEAD,
};

struct ei_region {
	struct object object;
	void *user_data;
	struct list link;
	uint32_t x, y;
	uint32_t width, height;
	double physical_scale;
};

struct ei_device {
	struct object object;
	void *user_data;
	struct list link;
	uint32_t id;
	enum ei_device_state state;
	uint32_t capabilities;
	char *name;
	enum ei_device_type type;

	struct list pending_event_queue; /* incoming events waiting for a frame */

	bool send_frame_event;

	uint32_t width, height;

	struct list regions;

	struct {
		bool x_is_stopped, y_is_stopped;
		bool x_is_cancelled, y_is_cancelled;
	} scroll;

	struct ei_keymap *keymap;
};

struct ei_keymap {
	struct object object;
	struct ei_device *device;
	void *user_data;
	enum ei_keymap_type type;
	int fd;
	size_t size;
	bool assigned;
};

struct ei_touch {
	struct object object;
	struct ei_device *device;
	void *user_data;
	uint32_t tracking_id;
	enum {
		TOUCH_IS_NEW,
		TOUCH_IS_DOWN,
		TOUCH_IS_UP,
	} state;

	double x, y;
};

struct ei_xkb_modifiers {
	uint32_t depressed;
	uint32_t latched;
	uint32_t locked;
	uint32_t group;
};

struct ei_event {
	struct object object; /* Parent is struct ei */
	enum ei_event_type type;
	struct list link;
	struct ei_seat *seat; /* NULL if device is non-NULL */
	struct ei_device *device;

	uint64_t timestamp;

	union {
		struct ei_xkb_modifiers modifiers;
		struct {
			char *name;
			char *value;
			uint32_t permissions;
		} prop;
		struct {
			double dx, dy; /* relative motion */
			double absx, absy; /* absolute motion */
			double sx, sy; /* scroll */
			int32_t sdx, sdy; /* discrete scroll */
			bool stop_x, stop_y; /* scroll stop */
			uint32_t button;
			bool button_is_press;
		} pointer;
		struct {
			uint32_t key;
			bool key_is_press;
		} keyboard;
		struct {
			uint32_t touchid;
			double x, y;
		} touch;
	};
};

struct ei_property {
	struct object object;
	struct list link;
	char *name;
	char *value;
	uint32_t permissions;
};

struct ei_property *
ei_property_unref(struct ei_property *prop);

struct ei_property *
ei_find_property(struct ei *ei, const char *name);

int
ei_property_update(struct ei *ei, const char *name,
		   const char *value, uint32_t permissions);

struct ei_event *
ei_event_new(struct ei *ei);

struct ei_event *
ei_event_new_for_device(struct ei_device *device);

struct ei *
ei_event_get_context(struct ei_event *event);

struct ei_event *
ei_event_ref(struct ei_event *event);

const char *
ei_event_type_to_string(enum ei_event_type type);

int
ei_set_connection(struct ei *ei, int fd);

void
ei_disconnect(struct ei *ei);

struct ei_seat *
ei_seat_new(struct ei *ei, uint32_t id, const char *name,
	    uint32_t capabilities);

struct ei_device *
ei_seat_find_device(struct ei_seat *seat, uint32_t deviceid);

void
ei_seat_remove(struct ei_seat *seat);

void
ei_seat_drop(struct ei_seat *seat);

int
ei_send_property(struct ei *ei, const char *name, const char *value, uint32_t permissions);

int
ei_send_seat_bind(struct ei_seat *seat, uint32_t capabilities);

int
ei_send_close_device(struct ei_device *device);

int
ei_send_start_emulating(struct ei_device *device);

int
ei_send_stop_emulating(struct ei_device *device);

int
ei_send_frame(struct ei_device *device, uint64_t time);

void
ei_queue_device_removed_event(struct ei_device *device);

void
ei_insert_device_removed_event(struct ei_device *device);

void
ei_queue_seat_removed_event(struct ei_seat *seat);

void
ei_queue_device_start_emulating_event(struct ei_device *device);

void
ei_queue_device_stop_emulating_event(struct ei_device *device);

void
ei_queue_frame_event(struct ei_device *device, uint64_t time);

void
ei_queue_pointer_rel_event(struct ei_device *device, double x, double y);

void
ei_queue_pointer_abs_event(struct ei_device *device, double x, double y);

void
ei_queue_pointer_button_event(struct ei_device *device, uint32_t button, bool is_press);

void
ei_queue_keyboard_key_event(struct ei_device *device, uint32_t key, bool is_press);

void
ei_queue_pointer_scroll_event(struct ei_device *device, double x, double y);

void
ei_queue_pointer_scroll_discrete_event(struct ei_device *device, int32_t x, int32_t y);

void
ei_queue_pointer_scroll_stop_event(struct ei_device *device, bool x, bool y);

void
ei_queue_pointer_scroll_cancel_event(struct ei_device *device, bool x, bool y);

void
ei_queue_touch_down_event(struct ei_device *device, uint32_t touchid,
			   double x, double y);

void
ei_queue_touch_motion_event(struct ei_device *device, uint32_t touchid,
			     double x, double y);

void
ei_queue_touch_up_event(struct ei_device *device, uint32_t touchid);

struct ei_device *
ei_device_new(struct ei_seat *seat, uint32_t deviceid);

void
ei_device_add_region(struct ei_device *device, struct ei_region *r);

void
ei_device_done(struct ei_device *device);

void
ei_device_removed_by_server(struct ei_device *device);

int
ei_device_event_frame(struct ei_device *device, uint64_t time);

void
ei_device_event_start_emulating(struct ei_device *device);

void
ei_device_event_stop_emulating(struct ei_device *device);

int
ei_device_event_pointer_rel(struct ei_device *device,
			    double x, double y);

int
ei_device_event_pointer_abs(struct ei_device *device,
			    double x, double y);

int
ei_device_event_pointer_button(struct ei_device *device,
			       uint32_t button, bool is_press);

int
ei_device_event_pointer_scroll(struct ei_device *device,
			       double x, double y);
int
ei_device_event_pointer_scroll_stop(struct ei_device *device, bool x, bool y);

int
ei_device_event_pointer_scroll_cancel(struct ei_device *device, bool x, bool y);

int
ei_device_event_pointer_scroll_discrete(struct ei_device *device,
					int32_t x, int32_t y);

int
ei_device_event_keyboard_key(struct ei_device *device,
			     uint32_t key, bool is_press);

int
ei_device_event_touch_down(struct ei_device *device, uint32_t tid,
			   double x, double y);
int
ei_device_event_touch_motion(struct ei_device *device, uint32_t tid,
			     double x, double y);
int
ei_device_event_touch_up(struct ei_device *device, uint32_t tid);

int
ei_send_pointer_rel(struct ei_device *device,
		    double x, double y);

int
ei_send_pointer_abs(struct ei_device *device,
		    double x, double y);

int
ei_send_pointer_button(struct ei_device *device,
		       uint32_t button, bool is_press);

int
ei_send_pointer_scroll(struct ei_device *device,
		       double x, double y);
int
ei_send_pointer_scroll_stop(struct ei_device *device, double x, double y);

int
ei_send_pointer_scroll_cancel(struct ei_device *device, double x, double y);

int
ei_send_pointer_scroll_discrete(struct ei_device *device,
				int32_t x, int32_t y);

int
ei_send_keyboard_key(struct ei_device *device,
		     uint32_t key, bool is_press);

int
ei_send_touch_down(struct ei_device *device, uint32_t tid,
		    double x, double y);
int
ei_send_touch_motion(struct ei_device *device, uint32_t tid,
		     double x, double y);
int
ei_send_touch_up(struct ei_device *device, uint32_t tid);

void
ei_device_added(struct ei_device *device);

void
ei_device_paused(struct ei_device *device);

void
ei_device_resumed(struct ei_device *device);

void
ei_device_set_type(struct ei_device *device, enum ei_device_type type);

void
ei_device_set_size(struct ei_device *device, uint32_t width, uint32_t height);

void
ei_device_set_name(struct ei_device *device, const char *name);

void
ei_device_set_seat(struct ei_device *device, const char *seat);

void
ei_device_set_capabilities(struct ei_device *device,
			   uint32_t capabilities);

void
ei_device_set_keymap(struct ei_device *device,
		     enum ei_keymap_type type,
		     int keymap_fd,
		     size_t size);

struct ei_region *
ei_region_new(void);

void
ei_region_set_size(struct ei_region *region, uint32_t w, uint32_t h);

void
ei_region_set_offset(struct ei_region *region, uint32_t x, uint32_t y);

void
ei_region_set_physical_scale(struct ei_region *region, double scale);

_printf_(6, 7) void
ei_log_msg(struct ei *ei,
	   enum ei_log_priority priority,
	   const char *file, int lineno, const char *func,
	   const char *format, ...);

_printf_(6, 0) void
ei_log_msg_va(struct ei *ei,
	      enum ei_log_priority priority,
	      const char *file, int lineno, const char *func,
	      const char *format,
	      va_list args);

#define log_debug(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_DEBUG, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_info(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_INFO, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_warn(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_WARNING, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_error(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_bug(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, "🪳  libei bug:  " __VA_ARGS__)
#define log_bug_client(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, "🪲  Bug: " __VA_ARGS__)
