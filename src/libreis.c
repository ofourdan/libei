/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <config.h>

#include "libreis.h"

#include "util-bits.h"
#include "util-object.h"
#include "util-macros.h"
#include "util-strings.h"
#include "util-mem.h"
#include "util-io.h"

#include "proto/ei.pb-c.h"

struct reis {
	struct object object;
	int eisfd;
};

static void
reis_destroy(struct reis *reis)
{
	xclose(reis->eisfd);
}

_public_
OBJECT_IMPLEMENT_UNREF_CLEANUP(reis);
static
OBJECT_IMPLEMENT_CREATE(reis);

static int
send_msg(int fd, const ClientMessage *msg)
{
	size_t msglen = client_message__get_packed_size(msg);
	Packet packet = PACKET__INIT;
	packet.length = msglen;
	size_t packetlen = packet__get_packed_size(&packet);

	uint8_t buf[packetlen + msglen];
	packet__pack(&packet, buf);
	client_message__pack(msg, buf + packetlen);
	return min(0, xsend(fd, buf, sizeof(buf)));
}

_public_ struct reis *
reis_new(int eisfd)
{
	_unref_(reis) *reis = reis_create(NULL);

	reis->eisfd = dup(eisfd);
	if (reis->eisfd == -1)
		return NULL;

	return steal(&reis);
}

#define prepare_msg(_type, _struct, _field) \
	ClientMessage msg = CLIENT_MESSAGE__INIT; \
	_struct _field = _type##__INIT; \
	msg.msg_case = CLIENT_MESSAGE__MSG_##_type; \
	msg._field = &_field

_public_ int
reis_set_property_with_permissions(struct reis *reis,
				   const char *name, const char *value,
				   uint32_t permissions)
{
	prepare_msg(PROPERTY, Property, property);

	property.name = (char*)name;
	property.value = value ? (char*)value : "";
	property.permissions = permissions;

	int rc = send_msg(reis->eisfd, &msg);
	return rc;
}

_public_ int
reis_set_name(struct reis *reis, const char *name)
{
	prepare_msg(CONFIGURE_NAME, ConfigureName, configure_name);
	configure_name.name = (char*)name;

	int rc = send_msg(reis->eisfd, &msg);
	return rc;
}

_public_ int
reis_allow_capability(struct reis *reis, enum reis_device_capability capability, ...)
{
	enum reis_device_capability cap = capability;
	uint32_t caps = 0;
	va_list args;

	va_start(args, capability);
	do {
		switch (cap) {
			case REIS_DEVICE_CAP_POINTER:
			case REIS_DEVICE_CAP_POINTER_ABSOLUTE:
			case REIS_DEVICE_CAP_KEYBOARD:
			case REIS_DEVICE_CAP_TOUCH:
				caps |= bit(cap);
				break;
			default:
				return -EINVAL;
		}
	} while ((cap = va_arg(args, enum reis_device_capability)) > 0);
	va_end(args);

	prepare_msg(CONFIGURE_CAPABILITIES, ConfigureCapabilities, configure_capabilities);
	configure_capabilities.allowed_capabilities = caps;

	int rc = send_msg(reis->eisfd, &msg);
	if (rc)
		return rc;

	return -EINVAL;
}
