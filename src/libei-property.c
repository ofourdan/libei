/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2021 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#include "config.h"

#include <errno.h>

#include "util-strings.h"
#include "util-object.h"

#include "libei-private.h"

static void
ei_property_destroy(struct ei_property *prop)
{
	list_remove(&prop->link);
	free(prop->name);
	free(prop->value);
}

static
OBJECT_IMPLEMENT_CREATE(ei_property);
OBJECT_IMPLEMENT_UNREF_CLEANUP(ei_property);


#define _PERM(p_, mask_) (((p_) & (mask_)) == (mask_))
#define PERM_R(p_) _PERM((p_)->permissions, EI_PROPERTY_PERM_READ)
#define PERM_RW(p_) _PERM((p_)->permissions, EI_PROPERTY_PERM_READ|EI_PROPERTY_PERM_WRITE)
#define PERM_RD(p_) _PERM((p_)->permissions, EI_PROPERTY_PERM_READ|EI_PROPERTY_PERM_DELETE)

struct ei_property *
ei_find_property(struct ei *ei, const char *name)
{
	struct ei_property *prop;
	list_for_each(prop, &ei->properties, link) {
		if (streq(prop->name, name)) {
			return prop;
		}
	}

	return NULL;
}

static struct ei_property *
ei_property_new(struct ei *ei, const char *name,
		const char *value, uint32_t permissions)
{
	struct ei_property *prop = ei_property_create(NULL);

	prop->name = xstrdup(name);
	prop->value = xstrdup(value);
	prop->permissions = permissions & EI_PROPERTY_PERM_ALL;

	/* Initial ref is owned by this list */
	list_append(&ei->properties, &prop->link);

	return prop;
}

static int
ei_property_delete(struct ei *ei, struct ei_property *prop)
{
	if (!PERM_RD(prop))
		return -EACCES;

	free(prop->value);
	prop->value = NULL;
	ei_property_unref(prop);

	return 0;
}

static int
ei_property_change(struct ei *ei, struct ei_property *prop,
		   const char *value, uint32_t permissions)
{
	if (!PERM_RW(prop))
		return -EACCES;

	if (prop->value != value) {
		free(prop->value);
		prop->value = xstrdup(value);
	}
	prop->permissions = permissions;

	return 0;
}

int
ei_property_update(struct ei *ei, const char *name,
		   const char *value, uint32_t permissions)
{
	struct ei_property *prop = ei_find_property(ei, name);
	int rc = 0;

	if (!prop) {
		ei_property_new(ei, name, value, permissions);
	} else if (value) {
		rc = ei_property_change(ei, prop, value, permissions);
	} else {
		rc = ei_property_delete(ei, prop);
	}

	return rc;
}

_public_ int
ei_property_set_with_permissions(struct ei *ei, const char *name, const char *value,
				 uint32_t permissions)
{
	if (strstartswith(name, "ei.") || strstartswith(name, "eis."))
		return -EACCES;

	struct ei_property *prop = ei_find_property(ei, name);
	if (prop && permissions && (prop->permissions | permissions) != prop->permissions)
		return -EPERM;

	int rc = ei_property_update(ei, name, value, permissions);
	if (rc == 0)
		ei_send_property(ei, name, value, permissions);

	return rc;
}

_public_ int
ei_property_set(struct ei *ei, const char *name, const char *value)
{
	uint32_t permissions = EI_PROPERTY_PERM_ALL;
	struct ei_property *prop = ei_find_property(ei, name);

	if (prop)
		permissions = prop->permissions;

	return ei_property_set_with_permissions(ei, name, value, permissions);
}

_public_ uint32_t
ei_property_get_permissions(struct ei *ei, const char *name)
{
	struct ei_property *prop = ei_find_property(ei, name);

	return (prop && PERM_R(prop)) ? prop->permissions : 0;
}

_public_ const char *
ei_property_get(struct ei *ei, const char *name)
{
	struct ei_property *prop = ei_find_property(ei, name);

	return (prop && PERM_R(prop)) ? prop->value : NULL;
}
