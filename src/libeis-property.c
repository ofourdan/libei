/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2021 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#include "config.h"

#include <errno.h>

#include "util-strings.h"
#include "util-object.h"

#include "libeis-private.h"
#include "libeis-proto.h"

static void
eis_property_destroy(struct eis_property *prop)
{
	list_remove(&prop->link);
	free(prop->name);
	free(prop->value);
}

static
OBJECT_IMPLEMENT_CREATE(eis_property);
OBJECT_IMPLEMENT_UNREF_CLEANUP(eis_property);

#define _PERM(p_, mask_) (((p_) & (mask_)) == (mask_))
#define PERM_R(p_) _PERM((p_)->permissions, EIS_PROPERTY_PERM_READ)
#define PERM_RW(p_) _PERM((p_)->permissions, EIS_PROPERTY_PERM_READ|EIS_PROPERTY_PERM_WRITE)
#define PERM_RD(p_) _PERM((p_)->permissions, EIS_PROPERTY_PERM_READ|EIS_PROPERTY_PERM_DELETE)

static struct eis_property *
eis_property_find(struct eis_client *client, const char *name)
{
	struct eis_property *prop;
	list_for_each(prop, &client->properties, link) {
		if (streq(prop->name, name)) {
			return prop;
		}
	}

	return NULL;
}

static struct eis_property *
eis_client_property_new(struct eis_client *client, const char *name,
			const char *value, uint32_t permissions)
{
	assert(name);
	assert(value);

	struct eis_property *prop = eis_property_create(NULL);
	prop->name = xstrdup(name);
	prop->value = xstrdup(value);
	prop->permissions = permissions & EIS_PROPERTY_PERM_ALL;

	/* initial ref is owned by this list */
	list_append(&client->properties, &prop->link);

	return prop;
}

static void
eis_property_delete(struct eis_client *client, struct eis_property *prop)
{
	eis_property_unref(prop);
}

static void
eis_property_change(struct eis_client *client, struct eis_property *prop,
		    const char *value, uint32_t permissions)
{
	if (prop->value != value) {
		free(prop->value);
		prop->value = xstrdup(value);
	}
	prop->permissions = permissions;
}

void
eis_property_update_from_client(struct eis_client *client, const char *name,
				const char *value, uint32_t permissions)
{

	/* Client can't set eis. namespace properties */
	if (strstartswith(name, "eis."))
		return;

	struct eis_property *prop = eis_property_find(client, name);
	if (!prop) {
		eis_client_property_new(client, name, value, permissions);
		return;
	}

	if (!PERM_RW(prop))
		return;

	if (!value) {
		if (PERM_RD(prop))
			eis_property_delete(client, prop);
		return;
	}

	/* Permissions can only be lowered by the client */
	permissions = prop->permissions & permissions;
	eis_property_change(client, prop, value, permissions);
}

static void
eis_property_update(struct eis_client *client, const char *name,
		    const char *value, uint32_t permissions)
{
	struct eis_property *prop = eis_property_find(client, name);

	if (!prop) {
		eis_client_property_new(client, name, value, permissions);
	} else if (value) {
		eis_property_change(client, prop, value, permissions);
	} else {
		eis_property_delete(client, prop);
	}
}

_public_ void
eis_client_property_set_with_permissions(struct eis_client *client, const char *name,
					 const char *value, uint32_t permissions)
{
	if (strstartswith(name, "ei.") || strstartswith(name, "eis."))
		return;

	eis_property_update(client, name, value, permissions);

	/* We should have a check to see if anything actually changed, but meh */
	struct eis *eis = eis_client_get_context(client);
	eis->requests->property(client, name, value, permissions);
}

_public_ void
eis_client_property_set(struct eis_client *client, const char *name, const char *value)
{
	uint32_t permissions = EIS_PROPERTY_PERM_ALL;
	struct eis_property *prop = eis_property_find(client, name);

	if (prop)
		permissions = prop->permissions;

	return eis_client_property_set_with_permissions(client, name, value, permissions);
}

_public_ uint32_t
eis_client_property_get_permissions(struct eis_client *client, const char *name)
{
	struct eis_property *prop = eis_property_find(client, name);

	return prop ? prop->permissions : 0;
}

_public_ const char *
eis_client_property_get(struct eis_client *client, const char *name)
{
	struct eis_property *prop = eis_property_find(client, name);

	return prop ? prop->value : NULL;
}
